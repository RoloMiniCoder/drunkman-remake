package org.academiadecodigo.offstrings.phases.game;

import org.academiadecodigo.offstrings.Engine;
import org.academiadecodigo.offstrings.configs.ResourcePaths;
import org.academiadecodigo.offstrings.phases.GamePhase;
import org.academiadecodigo.offstrings.phases.end.GameEnd;
import org.academiadecodigo.offstrings.phases.game.gameobjects.Player;
import org.academiadecodigo.offstrings.phases.game.gameobjects.eatables.Eatable;
import org.academiadecodigo.offstrings.phases.game.gameobjects.eatables.EatableFactory;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Game extends GamePhase {
    private static final int EATABLE_AMOUNT = 30;

    private GameEnd gameEnd;
    private List<Eatable> eatables;
    private String player1path;
    private String player2path;
    private Player player1;
    private Player player2;
    private boolean gameOver;

    public Game(Engine engine) {
        super(engine, ResourcePaths.GAME_MUSIC);
        eatables = null;
        gameOver = false;
    }

    @Override
    public void execute() throws InterruptedException {
        background.draw();
        eatables = createEatables(EATABLE_AMOUNT);
        player1 = new Player(player1path);
        player1.show();
        player2 = new Player(player2path);
        player2.show();
        music.play(true);
        music.setLoop(Integer.MAX_VALUE);

        while (!gameOver) {
            Thread.sleep(10);
            if(eatables.size() == 0){
                gameOver = true;
            }
        }

        gameEnd.setWinner(player1.getScore() > player2.getScore()
                ? ResourcePaths.PLAYER1
                : ResourcePaths.PLAYER2);
        gameEnd.setWinnerPicturePath(player1.getScore() > player2.getScore()
                ? player1path
                : player2path);
        engine.setCurrentPhase(gameEnd);
        cleanup();
    }

    private List<Eatable> createEatables(int amount) {
        List<Eatable> toReturn = new LinkedList<>();
        for (int i = 0; i < amount; i++) {
            Eatable e = EatableFactory.createEatable();
            e.show();
            toReturn.add(e);
        }
        return toReturn;
    }

    private void cleanup() {
        player1.delete();
        player2.delete();
        for (Eatable e : eatables) {
            e.delete();
        }
        eatables = null;
        gameOver = false;
        music.stop();
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_UP:
                player2.moveUp();
                checkCollision(player2);
                break;
            case KeyboardEvent.KEY_DOWN:
                player2.moveDown();
                checkCollision(player2);
                break;
            case KeyboardEvent.KEY_LEFT:
                player2.moveLeft();
                checkCollision(player2);
                break;
            case KeyboardEvent.KEY_RIGHT:
                player2.moveRight();
                checkCollision(player2);
                break;
            case KeyboardEvent.KEY_W:
                player1.moveUp();
                checkCollision(player1);
                break;
            case KeyboardEvent.KEY_S:
                checkCollision(player1);
                player1.moveDown();
                break;
            case KeyboardEvent.KEY_A:
                player1.moveLeft();
                checkCollision(player1);
                break;
            case KeyboardEvent.KEY_D:
                player1.moveRight();
                checkCollision(player1);
                break;
        }
    }

    public void checkCollision(Player player) {
        Iterator<Eatable> it = eatables.iterator();
        while (it.hasNext()) {
            Eatable e = it.next();
            if (player.isColliding(e)) {
                player.score(e.getPointsWorth());
                e.delete();
                it.remove();
                if (player.getScore() < 0) {
                    gameOver = true;
                }
            }
        }
    }

    public void setPlayers(String p1, String p2) {
        player1path = p1;
        player2path = p2;
    }

    public void setGameEnd(GameEnd gameEnd) {
        this.gameEnd = gameEnd;
    }
}
