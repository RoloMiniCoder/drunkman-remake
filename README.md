# Drunkman!

This is a from scratch refactor of one of my student's games.
Made as an instructional example to showcase some different strategies, thoughts and approaches at the same problem/behaviour, using only the knowledge they had at the time they were given the assignment.